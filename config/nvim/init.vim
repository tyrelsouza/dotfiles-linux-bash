syntax enable
let mapleader = " "

call plug#begin('~/.vim/plugged')
	Plug 'ervandew/supertab'
  Plug 'preservim/nerdtree'
  Plug 'liuchengxu/space-vim-dark'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
  Plug 'junegunn/fzf.vim'
  Plug 'mhinz/vim-startify'
  Plug 'jeetsukumaran/vim-buffergator'
	Plug 'ap/vim-buftabline'
  Plug 'airblade/vim-gitgutter'
	Plug 'majutsushi/tagbar'
	Plug 'vim-airline/vim-airline'
  " Plug 'ctrlpvim/ctrlp.vim' " Replaced with telescope
	Plug 'enricobacis/vim-airline-clock'
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim'
call plug#end()

map <C-o> :NERDTreeToggle<CR>

set tabstop=2
set shell=/bin/bash
set shiftwidth=2
set expandtab 
set hidden
set background=dark
set autoindent
set preserveindent
set copyindent

" Set Colors
if (has("termguicolors"))
 set termguicolors
endif
colorscheme space-vim-dark
hi Comment cterm=italic


" Relative numbers and when not in buiffer set absolute numbers
set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" Colors

" Disable EX Mode
map q: <Nop>
" Disable EX Mode
nnoremap Q <nop>

" Search with Ag
nnoremap <Leader>/ :Ag<CR>
nnoremap <C-J> :bnext<CR>
nnoremap <C-K> :bprev<CR>
nmap <leader>bq :bp <BAR> bd #<CR>

"Clock
let g:airline#extensions#clock#format = '%I:%M:%S %X'

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>


